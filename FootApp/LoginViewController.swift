//
//  ViewController.swift
//  FootApp
//
//  Created by Nihed Majdoub on 04/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController
{
    @IBOutlet weak var logoLabel: UILabel!
    
    let forgetPasswordlabel : TTTAttributedLabel = {
        let label = TTTAttributedLabel(frame: CGRectZero)
        label.font = UIFont.AvenirNextRegular(size: 12)
        label.textColor = UIColor.flatWhiteColor()
        label.textAlignment = .Center
        label.lineBreakMode = .ByWordWrapping
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.userInteractionEnabled = true
        
        let text = String.localize("forgot_pass")
        let subText = String.localize("reset_pass")
        
        if let boldRange = text.rangeOfString(subText, options: .CaseInsensitiveSearch)
        {
            let boldFont = UIFont.AvenirNextDemiBold(size: 13)
            let start = text.startIndex.distanceTo(boldRange.startIndex)
            let length = boldRange.startIndex.distanceTo(boldRange.endIndex)
            let range = NSMakeRange(start, length)
            label.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (attrString) -> NSMutableAttributedString! in
                attrString.addAttribute(NSFontAttributeName, value: boldFont, range: range)
                return attrString
            })
        }
        
        return label
    }()
    
    let loginTextField : UITextField = {
        let textField = UITextField(placeHolder: String.localize("login_text"))
        textField.keyboardType = UIKeyboardType.EmailAddress
        textField.returnKeyType = UIReturnKeyType.Next
        textField.autocapitalizationType = .None
        return textField
    }()
    
    let passwordTextField : UITextField = {
        let textField = UITextField(placeHolder: String.localize("pass_text"))
        textField.secureTextEntry = true
        textField.returnKeyType = UIReturnKeyType.Go
        return textField
    }()
    
    let loginButton : UIButton = {
        let button = UIButton()
        button.addBorder(width: 1.0, color: UIColor(white: 0.9, alpha: 0.3))
        button.setCornerRadius(radius: 4)
        button.setTitle(String.localize("login"), forState: .Normal)
        button.setTitleColor(UIColor.flatWhiteColor(), forState: .Normal)
        button.setTitleColor(UIColor(white: 0.9, alpha: 0.4), forState: .Disabled)
        button.setBackgroundColor(UIColor(white: 0.9, alpha: 0.3), forState: .Normal)
        button.titleLabel?.font = UIFont.AvenirNextDemiBold(size: 16)
        button.enabled = false
        return button
    }()
    
    let facebookButton : UIButton = {
        let button = UIButton()
        button.setTitle(String.localize("login_facebook") , forState: .Normal)
        button.setTitleColor(UIColor.flatWhiteColor(), forState: .Normal)
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.6), forState: .Highlighted)
        button.setBackgroundColor(UIColor.clearColor(), forState: .Normal)
        button.titleLabel?.font = UIFont.AvenirNextDemiBold(size: 16)
        button.setImage(UIImage(named: "ic_fb_white"), forState: .Normal)
        button.titleLabel?.textAlignment = .Center
        return button
    }()
    
    let separatorView : SeparatorView = {
        let view = SeparatorView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }()
    
    let signupView : BluryView = {
        let view = BluryView(type: BluryViewType.SignUp)
        return view
    }()
    
    var emailAlertView : EmailInputBox? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        configureViews()
        configureLayoutConstraints()
        
        
        ServicesManager.instance().autoLogin {[weak self] success in
            guard success else
            {
                ServicesManager.instance().logout({ (success) in
                    
                })
                return
            }
         
            self?.presentTabBar()
        }
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .Fade)
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    private func configureViews()
    {
        view.backgroundColor = UIColor(gradientStyle: .Radial, withFrame: view.bounds, andColors: [UIColor.flatOrangeColor(),UIColor.flatMagentaColor(),UIColor.flatRedColor()])
        
        view.addSubview(loginTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        view.addSubview(facebookButton)
        view.addSubview(forgetPasswordlabel)
        view.addSubview(separatorView)
        view.addSubview(signupView)
        
        loginTextField.delegate = self
        passwordTextField.delegate = self
        
        loginButton.addTarget(self, action: #selector(login), forControlEvents: .TouchUpInside)
        facebookButton.addTarget(self, action: #selector(loginWithFacebook), forControlEvents: .TouchUpInside)
        
        forgetPasswordlabel.addTapGesture {[weak self] (rec) in
            self?.showResetPasswordAlert()
        }
        
        signupView.addTapGesture {[weak self] (rec) in
            self?.performSegueWithIdentifier(kSignUpSegue, sender: self)
        }
    }
    
    private func configureLayoutConstraints()
    {
        loginTextField.snp_makeConstraints { (make) in
            make.top.equalTo(logoLabel.snp_bottom).offset(20)
            make.width.equalTo(view).multipliedBy(0.86)
            make.height.equalTo(view).multipliedBy(0.07)
            make.centerX.equalTo(view)
        }
        
        passwordTextField.snp_makeConstraints { (make) in
            make.width.equalTo(loginTextField)
            make.height.equalTo(loginTextField)
            make.centerX.equalTo(loginTextField)
            make.top.equalTo(loginTextField.snp_bottom).offset(20)
        }
        
        loginButton.snp_makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp_bottom).offset(20)
            make.width.equalTo(loginTextField)
            make.centerX.equalTo(loginTextField)
            make.height.equalTo(loginTextField)
        }
        
        forgetPasswordlabel.snp_makeConstraints { (make) in
            make.top.equalTo(loginButton.snp_bottom).offset(20)
            make.width.equalTo(loginTextField)
            make.centerX.equalTo(loginTextField)
        }
        
        separatorView.snp_makeConstraints { (make) in
            make.bottom.equalTo(forgetPasswordlabel.snp_bottom).offset(50)
            make.width.equalTo(facebookButton).multipliedBy(0.88)
            make.height.equalTo(30)
            make.centerX.equalTo(loginTextField)
        }
        
        facebookButton.snp_makeConstraints { (make) in
            make.top.equalTo(separatorView).offset(35)
            make.width.equalTo(loginTextField).multipliedBy(1.0)
            make.centerX.equalTo(loginTextField)
            make.height.equalTo(loginTextField)
        }
        
        signupView.snp_makeConstraints { (make) in
            make.bottom.equalTo(view)
            make.width.equalTo(view)
            make.centerX.equalTo(view)
            make.height.equalTo(view).multipliedBy(0.08)
        }
    }
    
    private func showResetPasswordAlert()
    {
        defer
        {
            emailAlertView!.show()
            view.userInteractionEnabled = false
        }
        guard emailAlertView == nil else {return}
        
        emailAlertView = EmailInputBox.boxWithStyle(.EmailInput, placeholder: String.localize("email_text"))
        emailAlertView!.blurEffectStyle = .Light
        emailAlertView!.title = String.localize("pass_forgotten")
        emailAlertView!.message = String.localize("enter_email")
        emailAlertView!.submitButtonText = String.localize("send_text")
        emailAlertView!.cancelButtonText = String.localize("cancel_text")
        emailAlertView!.validationLabelText = String.localize("valid_email")
        
        emailAlertView!.onSubmit = {[weak self](value: AnyObject...) in
            self?.view.userInteractionEnabled = true
            guard let email = value.first as? String else {return}
            self?.resetUserPassword(email: email)
        }
        emailAlertView!.onCancel = {[weak self] in
            self?.view.userInteractionEnabled = true
        }
    }
    
    func login()
    {
        guard shouldEnableLogin() else
        {
            Utilities.showErrorToast(String.localize("invalid_params"))
            return
        }
        
        guard ServicesManager.isInternetConnected() else
        {
            Utilities.showErrorToast(String.localize("no_internet"))
            return
        }
        
        ServicesManager.instance().login(login: loginTextField.text, password: passwordTextField.text) {[weak self] (success) in
            guard success else
            {
                return
            }
            self?.presentTabBar()
        }
    }
    
    func loginWithFacebook()
    {
        ServicesManager.instance().loginToFacebook {[weak self] (response , user) in
            
            guard let response = response else {return}
            
            guard let strongSelf = self, currentUser = ServicesManager.instance().currentUser() where response.success else
            {
                return
            }
            
            guard currentUser.avatarUrl == nil else
            {
                strongSelf.presentTabBar()
                return
            }
            
            ServicesManager.instance().updateFacebookProfileImage { (success) in
                guard success else {return}
                
                strongSelf.presentTabBar()
            }
        }
    }
    
    func resetUserPassword(email email: String)
    {
        guard ServicesManager.isInternetConnected() else
        {
            Utilities.showErrorToast(String.localize("no_internet"))
            return
        }
        
        ServicesManager.instance().resetPassword(email: email) {[weak emailAlertView] (success) in
            guard success else
            {
                Utilities.showErrorToast(String.localize("email_not_found"))
                return
            }
            emailAlertView?.hide()
            Utilities.showSuccessToast(String.localize("email_sent"))
        }
    }
    
    func presentTabBar()
    {
        ServicesManager.instance().subscribeToPushNotifications(forceSettings: true) { (success) in
            guard success else
            {
                ServicesManager.instance().settingsManager.pushNotificationsEnabled = false
                return
            }
        }
        
        self.performSegueWithIdentifier(kTabBarSegue, sender: nil)
    }
}

// MARK: FBSDKAppInviteDialog Delegate

extension LoginViewController : FBSDKAppInviteDialogDelegate
{
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!)
    {
        debugPrint("results \(results)")
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!)
    {
        debugPrint("error \(error)")
    }
}

// MARK: UITextField Delegates

extension LoginViewController : UITextFieldDelegate
{
    
    func shouldEnableLogin() -> Bool
    {
        return loginTextField.text?.length > 0 && passwordTextField.text?.length > 0
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == loginTextField
        {
            passwordTextField.becomeFirstResponder()
        }
        else if textField == passwordTextField
        {
            login()
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        loginButton.enabled = shouldEnableLogin()
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool
    {
        loginButton.enabled = false
        return true
    }
}