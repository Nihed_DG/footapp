//
//  AppDelegate.swift
//  FootApp
//
//  Created by Nihed Majdoub on 04/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        Fabric.with([Crashlytics.self, Digits.self])
        
        initializeQBService()
        customizeApp()
        
        ServicesManager.instance().notificationService.registerForRemoteNotification()
        
        if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary
        {
            ServicesManager.instance().notificationService.pushDialogID = remoteNotification["dialog_id"] as? String
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
        ServicesManager.instance().deviceToken = deviceToken
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
    {
        NSLog("Push failed to register with error: %@", error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        NSLog("my push is: %@", userInfo)
        guard application.applicationState == UIApplicationState.Inactive else
        {
            return
        }
        
        guard let dialogID = userInfo["dialog_id"] as? String where !dialogID.isEmpty else
        {
            return
        }
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        guard dialogWithIDWasEntered != dialogID else
        {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        dispatch_async(dispatch_get_main_queue(),{
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(self)
        });
    }
    
    func applicationWillResignActive(application: UIApplication)
    {
        
    }
    
    func applicationDidEnterBackground(application: UIApplication)
    {
        ServicesManager.instance().chatService.disconnectWithCompletionBlock(nil)
    }
    
    func applicationWillEnterForeground(application: UIApplication)
    {
        ServicesManager.instance().chatService.connectWithCompletionBlock(nil)
    }
    
    func applicationDidBecomeActive(application: UIApplication)
    {
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(application: UIApplication)
    {
        ServicesManager.instance().chatService.disconnectWithCompletionBlock(nil)
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        let urlWasIntendedForFacebook = FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return urlWasIntendedForFacebook;
    }
    
    // MARK: QuickBlox Methods
    
    private func initializeQBService()
    {
        QBManager.SetQBCredentials()
    }
    
    // MARK: Customize App
    
    private func customizeApp()
    {
        UITextField.appearance().tintColor = UIColor.flatWhiteColor()
        UINavigationBar.appearance().tintColor = UIColor.flatWhiteColor()
        UINavigationBar.appearance().barTintColor = UIColor.flatMintColorDark()
    }
}

// MARK: NotificationService Delegate

extension AppDelegate : NotificationServiceDelegate
{
    func notificationServiceDidStartLoadingDialogFromServer()
    {
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer()
    {
    }
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!)
    {
        debugPrint("notificationServiceDidSucceedFetchingDialog \(chatDialog)")
        /*
         let navigatonController: UINavigationController! = self.window?.rootViewController as! UINavigationController
         
         let chatController: ChatViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
         chatController.dialog = chatDialog
         
         let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
         if !dialogWithIDWasEntered.isEmpty
         {
         // some chat already opened, return to dialogs view controller first
         navigatonController.popViewControllerAnimated(false);
         }
         
         navigatonController.pushViewController(chatController, animated: true)
         */
    }
    
    func notificationServiceDidFailFetchingDialog()
    {
    }
}

