//
//  QBErrorHandlers.h
//  FootApp
//
//  Created by Nihed Majdoub on 20/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

#import <QMServices.h>

static NSString *const kQMErrorKey = @"errors";
static NSString *const kQMBaseKey = @"base";
static NSString *const kQMErrorEmailKey = @"email";
static NSString *const kQMErrorLoginKey = @"login";
static NSString *const kQMErrorFullNameKey = @"full_name";
static NSString *const kQMErrorPasswordKey = @"password";

@interface QBErrorHandlers : NSObject

+ (NSString*)handleErrorResponse:(QBResponse *)response authorized:(BOOL)isAuthorized;

@end
