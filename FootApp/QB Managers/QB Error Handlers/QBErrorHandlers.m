//
//  QBErrorHandlers.m
//  FootApp
//
//  Created by Nihed Majdoub on 20/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

#import "QBErrorHandlers.h"

@implementation QBErrorHandlers

+ (NSString*)handleErrorResponse:(QBResponse *)response authorized:(BOOL)isAuthorized
{
    
    NSString *errorMessage = [[NSString alloc] init];
    
    id errorReasons = response.error.reasons[kQMErrorKey];
    
    if (isAuthorized)
    {
        if ([errorReasons isKindOfClass:[NSDictionary class]] && errorReasons[kQMBaseKey] != nil)
        {
            
            errorMessage = [errorReasons[kQMBaseKey] firstObject];
        }
        else
        {
            errorMessage = [self errorStringFromResponseStatus:response.status];
        }
    }
    else
    {
        if ([errorReasons isKindOfClass:[NSDictionary class]])
        {
            if (errorReasons[kQMBaseKey] != nil)
            {
                errorMessage = [errorReasons[kQMBaseKey] firstObject];
            }
            else
            {
                if (errorReasons[kQMErrorLoginKey])
                {
                    NSString *errorString = [NSString stringWithFormat:NSLocalizedString(@"LOGIN_ERROR", nil), [self errorStringFromArray:errorReasons[kQMErrorLoginKey]]];
                    errorMessage = [self appendErrorString:errorString toMessageString:errorMessage];
                }
                
                if (errorReasons[kQMErrorEmailKey])
                {
                    NSString *errorString = [NSString stringWithFormat:NSLocalizedString(@"EMAIL_ERROR", nil), [self errorStringFromArray:errorReasons[kQMErrorEmailKey]]];
                    errorMessage = [self appendErrorString:errorString toMessageString:errorMessage];
                }
                
                if (errorReasons[kQMErrorFullNameKey])
                {
                    NSString *errorString = [NSString stringWithFormat:NSLocalizedString(@"FULL_NAME_ERROR", nil), [self errorStringFromArray:errorReasons[kQMErrorFullNameKey]]];
                    errorMessage = [self appendErrorString:errorString toMessageString:errorMessage];
                    
                }
                
                if (errorReasons[kQMErrorPasswordKey])
                {
                    NSString *passwordTooShort = NSLocalizedString(@"PASS_TOO_SHORT",nil);
                    //                    NSString *errorString = [NSString stringWithFormat:NSLocalizedString(@"PASSWORD_ERROR", nil), [self errorStringFromArray:errorReasons[kQMErrorPasswordKey]]];
                    NSString *errorString = [NSString stringWithFormat:NSLocalizedString(@"PASSWORD_ERROR", nil), passwordTooShort];
                    
                    errorMessage = [self appendErrorString:errorString toMessageString:errorMessage];
                }
            }
        }
        else
        {
            errorMessage = [self errorStringFromResponseStatus:response.status];
        }
    }
    
    return errorMessage;
}

+ (NSString *)errorStringFromResponseStatus:(QBResponseStatusCode)statusCode
{
    NSString *errorString = nil;
    
    switch (statusCode)
    {
        case QBResponseStatusCodeServerError:
            errorString = NSLocalizedString(@"BAD_GATEWAY_ERROR", nil);
            break;
        case QBResponseStatusCodeUnknown:
            errorString = NSLocalizedString(@"no_internet", nil);
            break;
        case QBResponseStatusCodeUnAuthorized:
            errorString = NSLocalizedString(@"INCORRECT_USER_DATA_ERROR", nil);
            break;
        case QBResponseStatusCodeValidationFailed:
            errorString = NSLocalizedString(@"INCORRECT_USER_DATA_ERROR", nil);
            break;
        default:
            errorString = NSLocalizedString(@"UNKNOWN_ERROR", nil);
            break;
    }
    
    return errorString;
}

+ (NSString *)errorStringFromArray:(NSArray *)errorArray
{
    NSString *errorString = [[NSString alloc] init];
    
    for (NSUInteger i = 0; i < errorArray.count; ++i)
    {
        if (i > 0)
        {
            errorString = [errorString stringByAppendingString:@" and "];
        }
        errorString = [errorString stringByAppendingString:errorArray[i]];
    }
    
    return errorString;
}

+ (NSString *)appendErrorString:(NSString *)errorString toMessageString:(NSString *)messageString
{
    if (messageString.length > 0)
    {
        messageString = [messageString stringByAppendingString:@"\n"];
    }
    
    messageString = [messageString stringByAppendingString:errorString];
    
    return messageString;
}


@end
