//
//  ServiceManager.swift
//  FootApp
//
//  Created by Nihed Majdoub on 05/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class ServicesManager: QMServicesManager
{
    var currentDialogID = ""
    
    private var contactListService : QMContactListService!
    let notificationService = NotificationService()
    let contentService = ContentService()
    let settingsManager = SettingsManager()
    var deviceToken: NSData?

    static let internetConnection = Reachability.reachabilityForInternetConnection()
    
    override init()
    {
        super.init()
        ServicesManager.internetConnection.startNotifier()
    }
    
    static func isInternetConnected() -> Bool
    {
        return internetConnection.isReachable()
    }
    
    func logout(completion: (success : Bool) -> ())
    {
        super.logoutWithCompletion
            {[weak self]  in
                guard let strongSelf = self else {return}
                
                strongSelf.settingsManager.clearSettings()
                strongSelf.logoutFromFacebook()
                strongSelf.notificationService.unSubscribeToPushNotifications({ (success) in
                    debugPrint("unSubscribeToPushNotifications \(success)")
                })
                completion(success: true)
        }
    }
    
    func login(login login: String?, password: String?, completion: (success: Bool) -> ())
    {
        let user = QBUUser()
        
        if let login = login where login.isEmail
        {
            user.email = login
        }
        else
        {
            user.login = login
        }
        user.password = password
        
        ServicesManager.instance().logInWithUser(user)
        {[weak self] (success, errorMessage) in
            
            if success
            {
                self?.settingsManager.setLogin(login, andPassword: password)
                self?.setAutoLogin(true, accountType: .LoginOrEmail)
            }
            completion(success: success)
        }
    }
    
    func setAutoLogin(autologin: Bool, accountType: QMAccountType)
    {
        settingsManager.rememberMe = autologin
        settingsManager.accountType = accountType
    }
    
    func autoLogin(completion: (success: Bool) -> ())
    {
        guard !isAuthorized() else
        {
            completion(success: true)
            return
        }
        
        if let password = settingsManager.password, login = settingsManager.login where  settingsManager.accountType == .LoginOrEmail
        {
            self.login(login: login, password: password) {(success) in
               completion(success: true)
            }
        }
        else if settingsManager.accountType == .Facebook
        {
            loginToFacebook({ (response, user) in
                
                completion(success: (response?.success)!)
            })
        }
        else
        {
            completion(success: false)
        }
    }
    
    func signUpAndLogin(login login: String?, password: String?, fullName: String?, profileImage: UIImage?, completion: (success: Bool) -> ())
    {
        
        let user = QBUUser()
        
        if let login = login where login.isEmail
        {
            user.email = login
        }
        else
        {
            user.login = login
        }
        
        user.password = password
        user.fullName = fullName
        user.tags = ["iOS"]
        
        ServicesManager.instance().authService.signUpAndLoginWithUser(user) {[weak self] (response, user) in
            
            guard response.success else
            {
                completion(success: false)
                return
            }
            

            self?.settingsManager.setLogin(login, andPassword: password)
            self?.setAutoLogin(true, accountType: .LoginOrEmail)

            
            if let profileImage = profileImage
            {
                ServicesManager.instance().updateCurrentUser(nil, image: profileImage, progress: { (progress) in
                    
                }) { (success) in
                    completion(success: success)
                }
            }
            else
            {
                completion(success: response.success)
            }            
        }
    }
    
    override func isAuthorized() -> Bool
    {
        return self.authService.isAuthorized;
    }
    
    override func handleErrorResponse(response: QBResponse)
    {
        guard let _ = response.error else {return}
        let errorMessage = QBErrorHandlers.handleErrorResponse(response, authorized: isAuthorized())
        Utilities.showErrorToast(errorMessage)
    }
}

// MARK: Facebook Methods

extension ServicesManager
{
    func loginToFacebook(completion : (response : QBResponse?, user : QBUUser?) -> ())
    {
        FacebookManager.connectToFacebook { (token) in
            guard let token = token else
            {
                completion(response: nil, user: nil)
                return
            }
            
            ServicesManager.instance().authService.logInWithFacebookSessionToken(token) { (response, user) in
                self.setAutoLogin(true, accountType: .Facebook)
                completion(response: response, user: user)
            }
        }
    }
    
    func getMyFacebookFriends(completion : (friends : [AnyObject]) -> ())
    {
        loginToFacebook { (response, user) in
            FacebookManager.fetchMyFriends({ (friends) in
                debugPrint("friends \(friends)")
            })
        }
    }
    
    func getMyFacebookProfile(completion : (myProfile : FBSDKProfile?, error : NSError?) -> ())
    {
        FacebookManager.loadMe { (myProfile,error) in
            completion(myProfile: myProfile,error: error)
        }
    }
    
    func inviteDialogWithDelegate(delegate : FBSDKAppInviteDialogDelegate)
    {
        FacebookManager.inviteFriendsWith(delegate)
    }
    
    func logoutFromFacebook()
    {
        FacebookManager.logout()
    }
    
    func updateFacebookProfileImage(completion: (success: Bool) -> ())
    {
        getMyFacebookProfile { (myProfile, error) in
            guard let myProfile = myProfile else {return}
            if let error = error
            {
                debugPrint("error loading profile \(error.localizedDescription)")
                completion(success: false)
            }
            else
            {
                let url = FacebookManager.generateAvatarUrl(myProfile)
                
                ServicesManager.instance().updateCurrentUser(nil, imageUrl: url, progress: { (progress) in
                    
                }) { (success) in
                    completion(success: success)
                }
            }
        }
    }
    
    func importFacebookPhoto(completion: (url: NSURL?) -> ())
    {
        FacebookManager.importFacebookProfileImage { (url) in
            completion(url: url)
        }
    }
}

// MARK: Users Methods

extension ServicesManager
{
    func updateCurrentUser(updateParams : QBUpdateUserParameters?, image : UIImage?, progress: ContentProgressBlock, completion: (success: Bool) -> ())
    {
        let updateUserProfile = {[weak self] (blob: QBCBlob?) -> () in
            guard let strongSelf = self ,currentUser = strongSelf.currentUser()   else {return}
            
            var newParams = updateParams
            if newParams == nil
            {
                newParams = QBUpdateUserParameters()
                newParams?.customData = currentUser.customData
            }
            
            if let blob = blob
            {
                if let url = blob.publicUrl()  where url.characters.count > 0
                {
                    newParams!.avatarUrl = url
                    newParams!.website = url
                }
                newParams!.blobID = Int(blob.ID)
            }
            
            let password = currentUser.password
            
            QBRequest.updateCurrentUser(newParams!, successBlock: { (response, user) in
                
                if response.success
                {
                    currentUser.password = password
                }
                completion(success: response.success)
                }, errorBlock: { (response) in
                    completion(success: response.success)
            })
        }
        
        guard let image = image else
        {
            updateUserProfile(nil)
            return
        }
        
        contentService.uploadJPEGImage(image, progress: progress) { (response, blob) in
            guard response.success else
            {
                updateUserProfile(nil);
                return
            }
            updateUserProfile(blob);
        }
    }
    
    func updateCurrentUser(params: QBUpdateUserParameters?, imageUrl: NSURL?, progress: ContentProgressBlock, completion: (success: Bool) -> ())
    {
        guard let imageUrl = imageUrl else {return}
        
        contentService.downloadFile(url: imageUrl) {[weak self] (data) in
            guard let strongSelf = self  else {return}
            
            let image = UIImage(data: data)
            strongSelf.updateCurrentUser(params, image: image, progress: progress, completion: completion)
        }
    }
    
    func resetPassword(email email: String, completion: (success: Bool) -> ())
    {
        QBRequest.resetUserPasswordWithEmail(email, successBlock: { (response) in
            completion(success: response.success)
        }) { (response) in
            completion(success: response.success)
        }
    }
}

// MARK: Notification Methods

extension ServicesManager
{
    func subscribeToPushNotifications(forceSettings forceSettings: Bool, completion: (success: Bool) -> ())
    {
        notificationService.subscribeToPushNotifications(forceSettings: forceSettings, completion: completion)
    }
    func unSubscribeToPushNotifications(completion: (success: Bool) -> ())
    {
        notificationService.unSubscribeToPushNotifications(completion)
    }
}