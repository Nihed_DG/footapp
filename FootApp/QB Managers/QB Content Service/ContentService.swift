//
//  ContentService.swift
//  FootApp
//
//  Created by MacBook Pro  on 08/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

struct ContentService
{
    private var contentOperationQueue : NSOperationQueue
    
    init()
    {
        contentOperationQueue = NSOperationQueue()
        contentOperationQueue.maxConcurrentOperationCount = 3;
    }
    
    func uploadJPEGImage(image : UIImage , progress : ContentProgressBlock, completion : FileUploadResponseBlock)
    {
        if let data = UIImageJPEGRepresentation(image, 0.4)
        {
            uploadData(data, fileName: "image", contentType: "image/jpeg", isPublic: true, progress: progress, completion: completion)
        }
    }
    
    func uploadPNGImage(image : UIImage , progress : ContentProgressBlock, completion : FileUploadResponseBlock)
    {
        if let data = UIImagePNGRepresentation(image)
        {
            uploadData(data, fileName: "image", contentType: "image/png", isPublic: true, progress: progress, completion: completion)
        }
    }
    
    func downloadFile(url url: NSURL, completion : (data : NSData) -> ())
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),{
            
            if let data = NSData(contentsOfURL: url)
            {
                dispatch_async(dispatch_get_main_queue(),{
                    completion(data: data)
                })
            }
        })
    }
    
    func downloadFile(blobID blobID : UInt, progress : ContentProgressBlock, completion : FileDownloadResponseBlock)
    {
        let downloadOperation =  DownloadContentOperation(blobID: blobID)
        downloadOperation.progressHandler = progress
        downloadOperation.downloadCompletion = completion
        
        contentOperationQueue.addOperation(downloadOperation)
    }
    
    //MARK: Private methods
    
    private func uploadData(data : NSData, fileName : String, contentType: String, isPublic: Bool, progress : ContentProgressBlock, completion : FileUploadResponseBlock)
    {
        let uploadOperation = UploadContentOperation(data: data, fileName: fileName, contentType: contentType, isPublic: isPublic)
        uploadOperation.progressHandler = progress
        uploadOperation.uploadCompletion = completion
        
        contentOperationQueue.addOperation(uploadOperation)
    }
    
    
}