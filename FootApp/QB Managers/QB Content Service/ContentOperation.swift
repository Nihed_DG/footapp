//
//  ContentOperation.swift
//  FootApp
//
//  Created by MacBook Pro  on 08/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

typealias ContentProgressBlock = (Float) -> ()

class ContentOperation : NSOperation
{
    var progressHandler : ContentProgressBlock? = nil
//    var completionHandler : AnyObject? = nil
    var cancelableRequest : QBRequest? = nil
    
    
    override func cancel()
    {
        cancelableRequest?.cancel()
    }
}