//
//  DownloadContentOperation.swift
//  FootApp
//
//  Created by MacBook Pro  on 08/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

typealias FileDownloadResponseBlock = (response : QBResponse, fileData : NSData?) -> ()

class DownloadContentOperation : ContentOperation
{
    private var blobID : UInt!
    
    var downloadCompletion : FileDownloadResponseBlock? = nil
    init(blobID : UInt)
    {
        super.init()
        self.blobID = blobID
    }
    
    override func main()
    {
        cancelableRequest = QBRequest .downloadFileWithID(blobID, successBlock: {[weak self] (response, data) in
            
            guard let strongSelf = self, block = strongSelf.downloadCompletion else
            {
                return
            }
            block(response: response, fileData: data)
            
            }, statusBlock: { (request, status) in
                guard let progressHandler = self.progressHandler , status = status else
                {
                    return
                }
                progressHandler(status.percentOfCompletion)
            }, errorBlock: {[weak self] (response) in
                guard let strongSelf = self, block = strongSelf.downloadCompletion else
                {
                    return
                }
                block(response: response, fileData: nil)
        })
    }
}