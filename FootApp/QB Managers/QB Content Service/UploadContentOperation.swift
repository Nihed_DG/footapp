//
//  UploadContentOperation.swift
//  FootApp
//
//  Created by MacBook Pro  on 08/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

typealias FileUploadResponseBlock = (response : QBResponse, blob : QBCBlob?) -> ()

class UploadContentOperation : ContentOperation
{
    private let data : NSData
    private let fileName : String
    private let contentType : String
    private let isPublic : Bool

    var uploadCompletion : FileUploadResponseBlock? = nil

    init(data : NSData, fileName: String, contentType: String, isPublic: Bool)
    {
        self.data = data
        self.fileName = fileName
        self.contentType = contentType
        self.isPublic = isPublic
        
        super.init()
    }
    
    override func main()
    {
        cancelableRequest = QBRequest .TUploadFile(data, fileName: fileName, contentType: contentType, isPublic: isPublic, successBlock: {[weak self] (response, blob) in
            
            guard let strongSelf = self, block = strongSelf.uploadCompletion else
            {
                return
            }
            block(response: response, blob: blob)
            }, statusBlock: { (request, status) in
                guard let progressHandler = self.progressHandler , status = status else
                {
                    return
                }
                progressHandler(status.percentOfCompletion)
            }, errorBlock: {[weak self] (response) in
                guard let strongSelf = self, block = strongSelf.uploadCompletion else
                {
                    return
                }
                block(response: response, blob: nil)
        })
    }
}