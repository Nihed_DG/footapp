//
//  QBManager.swift
//  FootApp
//
//  Created by Nihed Majdoub on 05/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import Foundation

struct QBManager
{
    static let kQBApplicationID:UInt = 40227
    static let kQBAuthKey = "rRBdadM24v6A563"
    static let kQBAuthSecret = "SVZFkAsfDuXaMek"
    static let kQBAccountKey = "RMzo8xDyjMUBecq76zTx"
    
    static func  SetQBCredentials()
    {
        QBSettings.setApplicationID(kQBApplicationID)
        QBSettings.setAuthKey(kQBAuthKey)
        QBSettings.setAuthSecret(kQBAuthSecret)
        QBSettings.setAccountKey(kQBAccountKey)
        
        QBSettings.setCarbonsEnabled(true)
        
        QBSettings.setLogLevel(QBLogLevel.Debug)
        QBSettings.enableXMPPLogging()
        
        QBSettings.setAutoReconnectEnabled(true)
        QBSettings.setNetworkIndicatorManagerEnabled(true)
    }
    
}