//
//  NotificationService.swift
//  FootApp
//
//  Created by Nihed Majdoub on 05/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import Foundation

protocol NotificationServiceDelegate
{
    /**
     *  Is called when dialog fetching is complete and ready to return requested dialog
     *
     *  @param chatDialog QBChatDialog instance. Successfully fetched dialog
     */
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!)
    
    /**
     *  Is called when dialog was not found nor in memory storage nor in cache
     *  and NotificationService started requesting dialog from server
     */
    func notificationServiceDidStartLoadingDialogFromServer()
    
    /**
     *  Is called when dialog request from server was completed
     */
    func notificationServiceDidFinishLoadingDialogFromServer()
    
    /**
     *  Is called when dialog was not found in both memory storage and cache
     *  and server request return nil
     */
    func notificationServiceDidFailFetchingDialog()
}

/**
 *  Service responsible for working with push notifications
 */

class NotificationService
{
    var delegate: NotificationServiceDelegate?
    var pushDialogID: String?
    
    func registerForRemoteNotification()
    {
        let settings = UIUserNotificationSettings(forTypes: [.Badge , .Alert, .Sound], categories:[])
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func handlePushNotificationWithDelegate(delegate: NotificationServiceDelegate!)
    {
        guard let dialogID = self.pushDialogID else
        {
            return
        }
        
        guard !dialogID.isEmpty else
        {
            return
        }
        
        self.delegate = delegate;
        
        ServicesManager.instance().chatService.fetchDialogWithID(dialogID, completion:
            {
                [weak self] (chatDialog: QBChatDialog?) -> Void in
                guard let strongSelf = self else { return }
                //
                if (chatDialog != nil)
                {
                    strongSelf.pushDialogID = nil;
                    strongSelf.delegate?.notificationServiceDidSucceedFetchingDialog(chatDialog);
                }
                else
                {
                    //
                    strongSelf.delegate?.notificationServiceDidStartLoadingDialogFromServer()
                    ServicesManager.instance().chatService.loadDialogWithID(dialogID, completion: { (loadedDialog: QBChatDialog?) -> Void in
                        
                        guard let unwrappedDialog = loadedDialog else
                        {
                            strongSelf.delegate?.notificationServiceDidFailFetchingDialog()
                            return
                        }
                        
                        //
                        strongSelf.delegate?.notificationServiceDidFinishLoadingDialogFromServer()
                        
                        //
                        strongSelf.delegate?.notificationServiceDidSucceedFetchingDialog(unwrappedDialog)
                    })
                }
            })
    }
    
    func subscribeToPushNotifications(forceSettings forceSettings: Bool, completion: (success: Bool) -> ())
    {
        let pushNotificationsEnabled = ServicesManager.instance().settingsManager.pushNotificationsEnabled
        let shouldsubscribe = pushNotificationsEnabled || forceSettings
        guard let deviceToken = ServicesManager.instance().deviceToken where shouldsubscribe else
        {
            completion(success: false)
            return
        }
        
        let deviceIdentifier = UIDevice.currentDevice().identifierForVendor!.UUIDString
        let subscription = QBMSubscription()
        
        subscription.notificationChannel = QBMNotificationChannelAPNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        QBRequest.createSubscription(subscription, successBlock: {(response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            
            debugPrint("success subscribing deviceToken \(deviceToken)")

            if forceSettings
            {
                ServicesManager.instance().settingsManager.pushNotificationsEnabled = true
            }
            completion(success: true)
        }) { (response: QBResponse!) -> Void in
            debugPrint("error subscribing \(response.error?.description)")
            completion(success: false)
        }
    }
    
    func unSubscribeToPushNotifications(completion: (success: Bool) -> ())
    {
        let pushNotificationsEnabled = ServicesManager.instance().settingsManager.pushNotificationsEnabled
        
        guard pushNotificationsEnabled else
        {
            completion(success: true)
            return
        }
        
        let deviceIdentifier = UIDevice.currentDevice().identifierForVendor!.UUIDString
        
        QBRequest.unregisterSubscriptionForUniqueDeviceIdentifier(deviceIdentifier, successBlock: { (response) in
            
            ServicesManager.instance().settingsManager.pushNotificationsEnabled = false
            completion(success: true)
            }) { (error) in
                
                guard let _ = error else
                {
                    ServicesManager.instance().settingsManager.pushNotificationsEnabled = false
                    completion(success: true)
                    return
                }
                completion(success: false)
        }
    }
}