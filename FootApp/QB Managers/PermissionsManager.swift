//
//  PermissionsManager.swift
//  FootApp
//
//  Created by Nihed Majdoub on 24/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class PermissionsManager
{
    static let sharedInstance = PermissionsManager()
    
    
    func showLibraryPermissionDialog(type: UIImagePickerControllerSourceType, completion: (granted: Bool) -> ())
    {
        let singlePscope = PermissionScope()
        
        if type == .Camera
        {
            singlePscope.addPermission(CameraPermission(), message: String.localize("access_camera"))
        }
        else if type == .PhotoLibrary
        {
            singlePscope.addPermission(PhotosPermission(), message: String.localize("access_photos"))
        }

        singlePscope.show({ (finished, results) in
            
            guard let status = results.first?.status else
            {
                completion(granted: false)
                return
            }
            
            if status == .Authorized
            {
                completion(granted: true)
            }
            else if status == .Unauthorized
            {
                completion(granted: false)
            }
            else
            {
                completion(granted: false)
            }
            
        }) { (results) in
            completion(granted: false)
        }
    }
}
