//
//  QBFacebookManager.swift
//  FootApp
//
//  Created by Nihed Majdoub on 05/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

struct FacebookManager
{
    private static let kFBGraphGetBase = "https://graph.facebook.com/"
    private static let kFBGraphGetPictureFormat = "/picture?height=100&width=100&access_token="
    private static let permissions = ["email", "public_profile", "user_friends"]
    private static let kFriendsPath = "me/friends"
    private static let kMePath = "me"
    
    private static let kDataKey = "data"
    private static let kIDKey = "id"
    
    private static let kAppUrl = "http://google.com"
    private static let kLogoUrl = "https://files.quickblox.com/ic_launcher.png"
    private static let kAppName = "FootApp"
    
    static func connectToFacebook(completion : (token : String?) -> ())
    {
        if let session = FBSDKAccessToken.currentAccessToken()
        {
            completion(token: session.tokenString)
        }
        else
        {
            let loginManager = FBSDKLoginManager()
            
            loginManager.logInWithReadPermissions(permissions, fromViewController: nil, handler: { (result, error) in
                if let error = error
                {
                    debugPrint("error \(error)")
                    return
                }
                
                guard !result.isCancelled else
                {
                    completion(token: nil)
                    return
                }
                
                completion(token: result.token.tokenString)
            })
        }
    }
    
    static func fetchMyFriends(completion : (friends : [AnyObject]) -> ())
    {
        guard FBSDKAccessToken.currentAccessToken() != nil else
        {
            completion(friends: [])
            return
        }
        
        let friendsRequest = FBSDKGraphRequest.init(graphPath:kFriendsPath , parameters:[:])
        
        friendsRequest.startWithCompletionHandler { (connection, result, error) in
            
            if error != nil
            {
                debugPrint("error fetching friend \(error.localizedDescription)")
                completion(friends: [])
            }
            else
            {
                guard let myFriends = result[kDataKey] as? Array<AnyObject> else
                {
                    completion(friends: [])
                    return
                }
                completion(friends: myFriends)
            }
        }
    }
    
    static func fetchMyFriendsIDs(completion : (friendsIDs : [AnyObject]) -> ())
    {
        fetchMyFriends { (friends) in
            let friendsIDs = friends.map({ (friend) -> String in
                
                guard let userID = friend[kIDKey] as? String else
                {
                    return ""
                }
                return userID
            })
            completion(friendsIDs: friendsIDs)
        }
    }
    
    static func inviteFriendsWith(delegate : FBSDKAppInviteDialogDelegate)
    {
        let content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: kAppUrl)
        content.appInvitePreviewImageURL = NSURL(string: kLogoUrl)
        
        FBSDKAppInviteDialog.showFromViewController(nil, withContent: content, delegate: delegate)
    }
    
    static func generateAvatarUrl(myProfile : FBSDKProfile) -> NSURL?
    {
        let url = myProfile.imageURLForPictureMode(FBSDKProfilePictureMode.Normal, size: CGSize(width: 100, height: 100))
        return url
    }
    
    static func loadMe(completion : (myProfile : FBSDKProfile?, error : NSError?) -> ())
    {
        if (FBSDKProfile.currentProfile() != nil)
        {
            completion(myProfile: FBSDKProfile.currentProfile(), error: nil)
        }
        else
        {
            connectToFacebook({ (token) in
                FBSDKProfile.loadCurrentProfileWithCompletion { (profile, error) in
                    completion(myProfile: profile, error: error)
                }
            })
        }
    }
    
    static func importFacebookProfileImage(completion: (url: NSURL?) -> ())
    {
        loadMe({ (myProfile, error) in
            guard let myProfile = myProfile else
            {
                completion(url: nil)
                return
            }
            completion(url: generateAvatarUrl(myProfile))
        })
    }
    
    static func logout()
    {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
}