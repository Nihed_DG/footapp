//
//  SignUpViewController.swift
//  FootApp
//
//  Created by Nihed Majdoub on 19/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController
{
    let addPhotoImageView : QMImageView = {
        let imageView = QMImageView(image: UIImage(named: "add_photo"))
        imageView.contentMode = .ScaleAspectFit
        imageView.imageViewType = .Circle
        imageView.tintColor = UIColor.flatWhiteColor()
        imageView.backgroundColor = UIColor.clearColor()
        return imageView
    }()
    
    let signupView : BluryView = {
        let view = BluryView(type: BluryViewType.LogIn)
        return view
    }()
    
    let userNameTextField : UITextField = {
        let textField = UITextField(placeHolder: String.localize("login_text"))
        textField.keyboardType = UIKeyboardType.EmailAddress
        textField.returnKeyType = UIReturnKeyType.Next
        textField.autocapitalizationType = .None
        return textField
    }()
    
    let passwordTextField : UITextField = {
        let textField = UITextField(placeHolder: String.localize("pass_text"))
        textField.secureTextEntry = true
        textField.returnKeyType = UIReturnKeyType.Go
        return textField
    }()
    
    let fullNameTextField : UITextField = {
        let textField = UITextField(placeHolder: String.localize("full_name"))
        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Go
        return textField
    }()
    
    let signUpButton : UIButton = {
        let button = UIButton()
        button.addBorder(width: 1.0, color: UIColor(white: 0.9, alpha: 0.3))
        button.setCornerRadius(radius: 4)
        button.setTitle(String.localize("signup"), forState: .Normal)
        button.setTitleColor(UIColor.flatWhiteColor(), forState: .Normal)
        button.setTitleColor(UIColor(white: 0.9, alpha: 0.4), forState: .Disabled)
        button.setBackgroundColor(UIColor(white: 0.9, alpha: 0.3), forState: .Normal)
        button.titleLabel?.font = UIFont.AvenirNextDemiBold(size: 16)
        button.enabled = false
        return button
    }()
    
    let separatorView : SeparatorView = {
        let view = SeparatorView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }()
    
    let facebookButton : UIButton = {
        let button = UIButton()
        button.setTitle(String.localize("login_facebook") , forState: .Normal)
        button.setTitleColor(UIColor.flatWhiteColor(), forState: .Normal)
        button.setTitleColor(UIColor(white: 1.0, alpha: 0.6), forState: .Highlighted)
        button.setBackgroundColor(UIColor.clearColor(), forState: .Normal)
        button.titleLabel?.font = UIFont.AvenirNextDemiBold(size: 16)
        button.setImage(UIImage(named: "ic_fb_white"), forState: .Normal)
        button.titleLabel?.textAlignment = .Center
        return button
    }()
    
    var profileImage : UIImage? = nil
    
    let imagePicker = QMImagePicker()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        configureViews()
        configureLayoutConstraints()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .Fade)
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        addPhotoImageView.addBorder(width: 2.0, color: UIColor.flatWhiteColor())
        addPhotoImageView.roundView()
    }
    
    private func configureViews()
    {
        view.backgroundColor = UIColor(gradientStyle: .Radial, withFrame: view.bounds, andColors: [UIColor.flatBlueColor(),UIColor.flatSkyBlueColor(),UIColor.flatSkyBlueColorDark()])
        
        view.addSubview(signupView)
        view.addSubview(addPhotoImageView)
        view.addSubview(userNameTextField)
        view.addSubview(passwordTextField)
        view.addSubview(fullNameTextField)
        view.addSubview(signUpButton)
        view.addSubview(separatorView)
        view.addSubview(facebookButton)
        
        userNameTextField.delegate = self
        passwordTextField.delegate = self
        
        signUpButton.addTarget(self, action: #selector(signUp), forControlEvents: .TouchUpInside)
        facebookButton.addTarget(self, action: #selector(loginWithFacebook), forControlEvents: .TouchUpInside)
        
        signupView.addTapGesture {[weak self] (rec) in
            self?.popVC()
        }
        
        addPhotoImageView.delegate = self
    }
    
    private func configureLayoutConstraints()
    {
        signupView.snp_makeConstraints { (make) in
            make.bottom.equalTo(view)
            make.width.equalTo(view)
            make.centerX.equalTo(view)
            make.height.equalTo(view).multipliedBy(0.08)
        }
        
        let isIphone5OrEalier = Utilities.isIphone5OrEalier()
        let addphotoOfsset = isIphone5OrEalier ? 20 : 40
        let heightMultiplier = isIphone5OrEalier ? 0.15 : 0.18
        addPhotoImageView.snp_makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.width.equalTo(addPhotoImageView.snp_height)
            make.height.equalTo(view).multipliedBy(heightMultiplier)
            make.top.equalTo(view).offset(addphotoOfsset)
        }
        
        userNameTextField.snp_makeConstraints { (make) in
            make.top.equalTo(addPhotoImageView.snp_bottom).offset(18)
            make.width.equalTo(view).multipliedBy(0.86)
            make.height.equalTo(view).multipliedBy(0.07)
            make.centerX.equalTo(view)
        }
        
        passwordTextField.snp_makeConstraints { (make) in
            make.width.equalTo(userNameTextField)
            make.height.equalTo(userNameTextField)
            make.centerX.equalTo(userNameTextField)
            make.top.equalTo(userNameTextField.snp_bottom).offset(20)
        }
        
        fullNameTextField.snp_makeConstraints { (make) in
            make.width.equalTo(userNameTextField)
            make.height.equalTo(userNameTextField)
            make.centerX.equalTo(userNameTextField)
            make.top.equalTo(passwordTextField.snp_bottom).offset(20)
        }
        
        signUpButton.snp_makeConstraints { (make) in
            make.top.equalTo(fullNameTextField.snp_bottom).offset(20)
            make.width.equalTo(userNameTextField)
            make.centerX.equalTo(userNameTextField)
            make.height.equalTo(userNameTextField)
        }
        
        separatorView.snp_makeConstraints { (make) in
            make.bottom.equalTo(signUpButton.snp_bottom).offset(60)
            make.width.equalTo(facebookButton).multipliedBy(0.88)
            make.height.equalTo(30)
            make.centerX.equalTo(userNameTextField)
        }
        
        facebookButton.snp_makeConstraints { (make) in
            make.top.equalTo(separatorView).offset(40)
            make.width.equalTo(userNameTextField).multipliedBy(1.0)
            make.centerX.equalTo(userNameTextField)
            make.height.equalTo(userNameTextField)
        }
    }
    
    func signUp()
    {
        guard shouldEnableSignup() else
        {
            Utilities.showErrorToast(String.localize("invalid_params"))
            return
        }
        
        guard ServicesManager.isInternetConnected() else
        {
            Utilities.showErrorToast(String.localize("no_internet"))
            return
        }
        
        ServicesManager.instance().signUpAndLogin(login: userNameTextField.text, password: passwordTextField.text, fullName: fullNameTextField.text, profileImage: profileImage)
        {[weak self] (success) in
            guard success else {return}
            self?.performSegueWithIdentifier(kTabBarSegue, sender: NSURL(string: ""))
        }
    }
    
    func loginWithFacebook()
    {
        ServicesManager.instance().loginToFacebook {[weak self] (response , user) in
            
            guard let response = response else {return}
            
            guard let strongSelf = self, currentUser = ServicesManager.instance().currentUser() where response.success else
            {
                return
            }
            
            guard currentUser.avatarUrl == nil else
            {
                strongSelf.performSegueWithIdentifier(kTabBarSegue, sender: nil)
                return
            }
            
            ServicesManager.instance().updateFacebookProfileImage { (success) in
                
                guard success else {return}
                
                strongSelf.performSegueWithIdentifier(kTabBarSegue, sender: nil)
            }
        }
    }
}

// MARK: UITextField Delegates

extension SignUpViewController : UITextFieldDelegate
{
    
    func shouldEnableSignup() -> Bool
    {
        return userNameTextField.text?.length > 0 && passwordTextField.text?.length >= Utilities.passwordMinimumLength
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == userNameTextField
        {
            passwordTextField.becomeFirstResponder()
        }
        else if textField == passwordTextField || textField == fullNameTextField
        {
            signUp()
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        signUpButton.enabled = shouldEnableSignup()
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool
    {
        guard textField != fullNameTextField else {return true}
        signUpButton.enabled = false
        return true
    }
}

extension SignUpViewController: QMImageViewDelegate
{
    func imageViewDidTap(imageView: QMImageView!)
    {
        self.dismissKeyboard()
        
        Utilities.showActionSheet(String.localize("Change Profile Photo"), buttonActions: getAlertActions())
    }
    
    func getAlertActions() -> [UIAlertAction]
    {
        let showImagePicker: (type: UIImagePickerControllerSourceType) -> () = {[weak self] type in
            
            self?.imagePicker.chooseSourceTypeInVC(self, allowsEditing: true, sourceType: type, result: { (image) in
                self?.profileImage = image
                self?.addPhotoImageView.applyImage(image)
            })
        }
        
        let permissionsblock: (type: UIImagePickerControllerSourceType) -> () = { type in
                PermissionsManager.sharedInstance.showLibraryPermissionDialog(type, completion: { (granted) in
                    guard granted else {return}
                    showImagePicker(type: type)
                })
        }
        
        
        let importFromFacebook = {
            
            ServicesManager.instance().importFacebookPhoto {[weak self] (url) in
                
                self?.addPhotoImageView.setImageWithURL(url, placeholder: self?.addPhotoImageView.image, options: .HighPriority, progress: { (received, total) in
                    
                    }, completedBlock: { (image, error, cacheType, url) in
                        self?.profileImage = image
                })
            }
        }
        
        var actions:[UIAlertAction]  = []
        let importAction = UIAlertAction(title: String.localize("import_facebook_avatar"), style: .Default) { (action) in
            ez.runThisInMainThread({
                importFromFacebook()
            })
        }
        actions.append(importAction)
        
        let takePhotoAction = UIAlertAction(title: String.localize("TAKE_NEW_PHOTO"), style: .Default) { (action) in
            permissionsblock(type: .Camera)
        }
        actions.append(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: String.localize("CHOOSE_FROM_LIBRARY"), style: .Default) { (action) in
            permissionsblock(type: .PhotoLibrary)

        }
        actions.append(choosePhotoAction)
        
        let cancelAction = UIAlertAction(title: String.localize("cancel_text"), style: .Cancel) { (action) in
            
        }
        actions.append(cancelAction)
        
        
        return actions
    }
}
