//
//  EditProfileViewController.swift
//  FootApp
//
//  Created by Nihed Majdoub on 06/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class ProfileViewController: MXSegmentedPagerController
{
    let currentUser = ServicesManager.instance().currentUser()!
    
    let urlString: String = {
        
        if let avatarUrl = ServicesManager.instance().currentUser()?.avatarUrl where avatarUrl.length > 0
        {
            return avatarUrl
        }
        return ""
    }()
    
    let imagePicker = QMImagePicker()
    var profileImage: UIImage? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
                
        configureViews()
        configureLayoutConstraints()
    }
    
    func configureViews()
    {
        let profileHeaderView = ProfileHeaderView.instanceFromNib()
        profileHeaderView?.profileImageView.delegate = self
        profileHeaderView?.updateImageViewwith(url: NSURL(string: urlString), completion: {[weak self] (image) in
            self?.profileImage = image
            
            })
        segmentedPager.parallaxHeader.view = profileHeaderView
        segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.Fill
        segmentedPager.parallaxHeader.height = 180
        segmentedPager.parallaxHeader.minimumHeight = 0
        
        segmentedPager.backgroundColor = UIColor.flatWhiteColor()
        segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown
        segmentedPager.segmentedControl.backgroundColor = UIColor.flatWhiteColor()
        let font = UIFont.AvenirNextDemiBold(size: 18)
        segmentedPager.segmentedControl.titleTextAttributes = [
            NSFontAttributeName: UIFont.AvenirNextDemiBold(size: 16), NSForegroundColorAttributeName : UIColor.flatBlueColorDark()]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSFontAttributeName: font, NSForegroundColorAttributeName : UIColor.flatMintColor()]
        segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleArrow
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.flatMintColor()
        segmentedPager.segmentedControl.selectionIndicatorHeight = 10
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
    }
    
    func configureLayoutConstraints()
    {
    }
    
    @IBAction func update(sender: AnyObject)
    {
        guard ServicesManager.isInternetConnected() else
        {
            Utilities.showErrorToast(String.localize("no_internet"))
            return
        }
        
        let params = QBUpdateUserParameters()
        params.customData = ServicesManager.instance().currentUser()?.customData
        
        ServicesManager.instance().updateCurrentUser(params, image: profileImage, progress: { (progress) in
            
        }) { (success) in
        }
    }
    
    @IBAction func logout(sender: AnyObject)
    {
        ServicesManager.instance().logout {[weak self] (success) in
            self?.tabBarController?.popVC()
        }
    }
    
    override func segmentedPager(segmentedPager: MXSegmentedPager, titleForSectionAtIndex index: Int) -> String
    {
        return ["About", "Friends"][index];
    }
    
    override func segmentedPager(segmentedPager: MXSegmentedPager, didScrollWithParallaxHeader parallaxHeader: MXParallaxHeader)
    {
        let progress = parallaxHeader.progress
        NSLog("progress %f", progress)
        
        guard let headerView = self.segmentedPager.parallaxHeader.view as? ProfileHeaderView else {return}
        
        if progress < 0.4
        {
            headerView.profileImageView.setScale(x: 1 + progress, y: 1 + progress)
        }
    }
}

extension ProfileViewController: QMImageViewDelegate
{
    func imageViewDidTap(imageView: QMImageView!)
    {
        Utilities.showActionSheet(String.localize("Change Profile Photo"), buttonActions: getAlertActions())
    }
    
    func getAlertActions() -> [UIAlertAction]
    {
        let showImagePicker: (type: UIImagePickerControllerSourceType) -> () = {[weak self] type in
            
            
            
            self?.imagePicker.chooseSourceTypeInVC(self, allowsEditing: true, sourceType: type, result: { (image) in
                guard let headerView = self?.segmentedPager.parallaxHeader.view as? ProfileHeaderView else {return}
                self?.profileImage = image
                headerView.applyImage(image)
                
            })
        }
        
        let permissionsblock: (type: UIImagePickerControllerSourceType) -> () = { type in
            PermissionsManager.sharedInstance.showLibraryPermissionDialog(type, completion: { (granted) in
                guard granted else {return}
                showImagePicker(type: type)
            })
        }
        
        
        let importFromFacebook = {
            
            ServicesManager.instance().importFacebookPhoto {[weak self] (url) in
                
                guard let headerView = self?.segmentedPager.parallaxHeader.view as? ProfileHeaderView else {return}
                headerView.updateImageViewwith(url: url, completion: { (image) in
                    self?.profileImage = image
                })
            }
        }
        
        var actions:[UIAlertAction]  = []
        let importAction = UIAlertAction(title: String.localize("import_facebook_avatar"), style: .Default) { (action) in
            ez.runThisInMainThread({
                importFromFacebook()
            })
        }
        actions.append(importAction)
        
        let takePhotoAction = UIAlertAction(title: String.localize("TAKE_NEW_PHOTO"), style: .Default) { (action) in
            permissionsblock(type: .Camera)
        }
        actions.append(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: String.localize("CHOOSE_FROM_LIBRARY"), style: .Default) { (action) in
            permissionsblock(type: .PhotoLibrary)
            
        }
        actions.append(choosePhotoAction)
        
        let cancelAction = UIAlertAction(title: String.localize("cancel_text"), style: .Cancel) { (action) in
            
        }
        actions.append(cancelAction)
        
        
        return actions
    }
    
    
}
