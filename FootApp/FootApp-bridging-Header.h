//
//  FootApp-bridging-Header.h
//  FootApp
//
//  Created by Nihed Majdoub on 05/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

#ifndef FootApp_bridging_Header_h
#define FootApp_bridging_Header_h

@import AVFoundation;
@import UIKit;

#import <QMServices.h>
#import <FBSDKCoreKit.h>
#import <FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "QBUpdateUserParameters+CustomData.h"
#import "QMChatViewController.h"
#import "QMImagePicker.h"
#import "Reachability.h"
#import "TTTAttributedLabel.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <DigitsKit/DigitsKit.h>
#import <Chameleon.h>
#import <CRToast.h>
#import "QBErrorHandlers.h"
#import "SSKeychain.h"
#import "SettingsManager.h"
#import "MXSegmentedPager.h"
#import "MXSegmentedPagerController.h"

//#import "QMChatContactRequestCell.h"
//#import "QMChatNotificationCell.h"
//#import "QMChatIncomingCell.h"
//#import "QMChatOutgoingCell.h"
//#import "QMCollectionViewFlowLayoutInvalidationContext.h"
//
//#import "TTTAttributedLabel.h"
//
//#import "TWMessageBarManager.h"
//
//#import "_CDMessage.h"
//#import "UIImage+QM.h"
//#import "UIColor+QM.h"
//
#endif /* FootApp_bridging_Header_h */
