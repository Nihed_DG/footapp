//
//  StringExt.swift
//  FootApp
//
//  Created by Nihed Majdoub on 19/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

extension String
{
    static func localize(text: String) -> String
    {
      return NSLocalizedString(text, comment: "")
    }
}