//
//  UITextFieldExt.swift
//  FootApp
//
//  Created by Nihed Majdoub on 18/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

extension UITextField
{
    convenience init(placeHolder: String,placeHolderColor : UIColor? = UIColor(white: 0.9, alpha: 0.8), backgroundColor: UIColor? = UIColor(white: 0.9, alpha: 0.3), radius: CGFloat? = 4)
    {
        self.init()
        self.backgroundColor = backgroundColor
        self.textColor = UIColor.flatWhiteColor()
        self.attributedPlaceholder = NSMutableAttributedString(string: placeHolder, attributes: [NSForegroundColorAttributeName: placeHolderColor!])
        self.setCornerRadius(radius: radius!)
        self.leftView = UIView(x: 0, y: 0, w: 15, h: 0)
        self.leftViewMode = UITextFieldViewMode.Always
        self.font = UIFont.AvenirNextRegular(size: 15)
        self.clearButtonMode = .Always
    }
}