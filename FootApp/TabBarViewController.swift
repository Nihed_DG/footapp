//
//  TabBarViewController.swift
//  FootApp
//
//  Created by Nihed Majdoub on 20/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func loadView()
    {
        super.loadView()
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .Fade)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
}
