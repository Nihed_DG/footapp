//
//  QMImagePicker.m
//  Q-municate
//
//  Created by Andrey Ivanov on 11.08.14.
//  Copyright (c) 2014 Quickblox. All rights reserved.
//

#import "QMImagePicker.h"
#import "REActionSheet.h"
#import <Photos/Photos.h>
#import "FootApp-Swift.h"

@interface QMImagePicker()

<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (copy, nonatomic) QMImagePickerResult result;

@end

@implementation QMImagePicker

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.delegate = self;
    }
    return self;
}

- (void)presentIn:(UIViewController *)vc configure:(void (^)(UIImagePickerController *picker))configure
           result:(QMImagePickerResult)result
{
    QMImagePicker *picker = [[QMImagePicker alloc] init];
    picker.result = result;
    configure(picker);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [vc presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *key = picker.allowsEditing ? UIImagePickerControllerEditedImage: UIImagePickerControllerOriginalImage;
    UIImage *image = info[key];
    __weak __typeof(self)weakSelf = self;
    [picker dismissViewControllerAnimated:YES completion:^
     {
         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
         weakSelf.result(image);
         weakSelf.result = nil;
     }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    __weak __typeof(self)weakSelf = self;
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        weakSelf.result = nil;
    }];
}

- (void)chooseSourceTypeInVC:(id)vc allowsEditing:(BOOL)allowsEditing sourceType:(UIImagePickerControllerSourceType)sourceType result:(QMImagePickerResult)result
{
    UIViewController *viewController = vc;
    
    [self presentIn:viewController configure:^(UIImagePickerController *picker)
     {
         picker.sourceType = sourceType;
         picker.allowsEditing = allowsEditing;
         
     } result:result];
}

@end
