//
//  Utilities.swift
//  FootApp
//
//  Created by MacBook Pro  on 18/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

public enum BluryViewType
{
    case SignUp
    case LogIn
}

let kTabBarSegue = "tabBarSegue"
let kSignUpSegue = "signUpSegue"

class Utilities
{
    static let passwordMinimumLength = 8
    
    static func showErrorToast(text: String?)
    {
        guard let text = text where text.length > 0 else {return}
        CRToastManager.dismissAllNotifications(false)
        let options = [kCRToastTextKey: text, kCRToastFontKey: UIFont.AvenirNextDemiBold(size: 15), kCRToastTextColorKey: UIColor.flatRedColor(), kCRToastNotificationTypeKey: CRToastType.NavigationBar.rawValue,kCRToastNotificationPresentationTypeKey: CRToastPresentationType.Cover.rawValue, kCRToastTextAlignmentKey:  NSTextAlignment.Center.rawValue, kCRToastBackgroundColorKey: UIColor.flatWhiteColor(), kCRToastAnimationInTypeKey: CRToastAnimationType.Spring.rawValue, kCRToastAnimationOutTypeKey: CRToastAnimationType.Gravity.rawValue, kCRToastAnimationInDirectionKey: CRToastAnimationDirection.Top.rawValue, kCRToastAnimationOutDirectionKey: CRToastAnimationDirection.Top.rawValue]
        
        CRToastManager.showNotificationWithOptions(options) {
            
        }
    }
    
    static func showSuccessToast(text: String?)
    {
        guard let text = text where text.length > 0 else {return}
        CRToastManager.dismissAllNotifications(false)
        let options = [kCRToastTextKey: text, kCRToastFontKey: UIFont.AvenirNextDemiBold(size: 15), kCRToastTextColorKey: UIColor.flatForestGreenColor(), kCRToastNotificationTypeKey: CRToastType.NavigationBar.rawValue,kCRToastNotificationPresentationTypeKey: CRToastPresentationType.Cover.rawValue, kCRToastTextAlignmentKey:  NSTextAlignment.Center.rawValue, kCRToastBackgroundColorKey: UIColor.flatWhiteColor(), kCRToastAnimationInTypeKey: CRToastAnimationType.Spring.rawValue, kCRToastAnimationOutTypeKey: CRToastAnimationType.Gravity.rawValue, kCRToastAnimationInDirectionKey: CRToastAnimationDirection.Top.rawValue, kCRToastAnimationOutDirectionKey: CRToastAnimationDirection.Top.rawValue]
        
        CRToastManager.showNotificationWithOptions(options) {
            
        }
    }
    
    static func showActionSheet(title: String, buttonActions: [UIAlertAction])
    {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .ActionSheet)
        
        for buttonAction in buttonActions
        {
            alertController.addAction(buttonAction)
        }
        alertController.show()
    }
    
    static func isIphone5OrEalier() -> Bool
    {
        return ez.screenHeight <= 568
    }
}