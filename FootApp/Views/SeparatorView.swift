//
//  SeparatorView.swift
//  FootApp
//
//  Created by Nihed Majdoub on 19/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class SeparatorView: UIView
{
    let separatorLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .Center
        label.font = UIFont.AvenirNextDemiBold(size: 15)
        label.textColor = UIColor.flatWhiteColor()
        label.text = String.localize("or_text")
        return label
    }()
    
    let rightView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.flatWhiteColor()
        return view
    }()
    
    let leftView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.flatWhiteColor()
        return view
    }()
    
    override init(frame : CGRect)
    {
        super.init(frame : frame)
        commonInit()
    }
    
    convenience init()
    {
        self.init(frame:CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("This class does not support NSCoding")
    }
    
    func commonInit()
    {
        self.addSubview(separatorLabel)
        self.addSubview(leftView)
        self.addSubview(rightView)

        separatorLabel.snp_makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.centerX.equalTo(self)
        }
        
        leftView.snp_makeConstraints { (make) in
            make.left.equalTo(self)
            make.right.equalTo(separatorLabel.snp_left).offset(-15)
            make.centerY.equalTo(separatorLabel)
            make.height.equalTo(1)
        }
        
        rightView.snp_makeConstraints { (make) in
            make.right.equalTo(self)
            make.left.equalTo(separatorLabel.snp_right).offset(15)
            make.centerY.equalTo(separatorLabel)
            make.height.equalTo(1)
        }
    }
}
