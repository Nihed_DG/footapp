//
//  ProfileHeaderView.swift
//  FootApp
//
//  Created by Nihed Majdoub on 31/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//


import UIKit

class ProfileHeaderView: UIView
{
    @IBOutlet weak var profileImageView: QMImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    let defaultImage = UIImage(named: "avatar_default")

    class func instanceFromNib() -> ProfileHeaderView?
    {
        guard let view = UINib(nibName: "ProfileHeaderView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as? ProfileHeaderView else
        {
            return nil
        }
        
        view.profileImageView.backgroundColor = UIColor.clearColor()
        view.profileImageView.imageViewType = .Circle
        view.profileImageView.addBorder(width: 2, color: UIColor.flatWhiteColor())
        view.profileImageView.contentMode = .ScaleAspectFit
        view.profileImageView.roundView()

        view.backgroundImageView.backgroundColor = UIColor.flatGrayColorDark()
        view.backgroundImageView.contentMode = .ScaleAspectFill
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        view.insertSubview(blurEffectView, belowSubview: view.profileImageView)
        
        return view
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    func applyImage(image: UIImage)
    {
        profileImageView.image = image
        backgroundImageView.image = image

    }
    
    func updateImageViewwith(url url: NSURL?, completion: (image: UIImage?) -> ())
    {
        guard let url = url else {return}
        
        profileImageView.setImageWithURL(url, placeholder: defaultImage, options: SDWebImageOptions.HighPriority, progress: { (receivedSize, expectedSize) in
        }) {[weak self](image, error, cacheType, url) in
            self?.backgroundImageView.image = image
            completion(image: image)
        }
    }
}
