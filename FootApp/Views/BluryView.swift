//
//  bluryView.swift
//  FootApp
//
//  Created by Nihed Majdoub on 19/05/2016.
//  Copyright © 2016 Nihed Majdoub. All rights reserved.
//

import UIKit

class BluryView: UIView
{
    let signUplabel : TTTAttributedLabel = {
        let label = TTTAttributedLabel(frame: CGRectZero)
        label.font = UIFont.AvenirNextRegular(size: 14)
        label.textColor = UIColor.flatWhiteColor()
        label.textAlignment = .Center
        label.lineBreakMode = .ByWordWrapping
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.userInteractionEnabled = true
        
        return label
    }()
    
    let lineView : UIView = {
    let view = UIView()
        view.backgroundColor = UIColor.flatWhiteColor()
        return view
    }()
    
    override init(frame : CGRect)
    {
        super.init(frame : frame)
        commonInit(BluryViewType.LogIn)
    }
    
    convenience init(type : BluryViewType)
    {
        self.init(frame:CGRect.zero)
        commonInit(type)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("This class does not support NSCoding")
    }
    
    func commonInit(type : BluryViewType)
    {
        self.backgroundColor = UIColor(white: 1.0, alpha: 0.15)
        
        var text = String.localize("")
        var subText = String.localize("")
        
        switch type
        {
        case BluryViewType.LogIn:
            text = String.localize("have_account")
            subText = String.localize("login")
        case BluryViewType.SignUp:
            text = String.localize("dont_have_account")
            subText = String.localize("signup")
        }
        
        if let boldRange = text.rangeOfString(subText, options: .CaseInsensitiveSearch)
        {
            let boldFont = UIFont.AvenirNextDemiBold(size: 15)
            let start = text.startIndex.distanceTo(boldRange.startIndex)
            let length = boldRange.startIndex.distanceTo(boldRange.endIndex)
            let range = NSMakeRange(start, length)
            signUplabel.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (attrString) -> NSMutableAttributedString! in
                attrString.addAttribute(NSFontAttributeName, value: boldFont, range: range)
                return attrString
            })
        }
        
        self.addSubview(signUplabel)
        self.addSubview(lineView)

        signUplabel.snp_makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.centerX.equalTo(self)
        }
        
        lineView.snp_makeConstraints { (make) in
            make.width.equalTo(self)
            make.centerX.equalTo(self)
            make.height.equalTo(0.4)
            make.top.equalTo(self)
        }
    }
}
